
# Classification in Pyspark
Develop a model that can predict that whether a car is in acceptable state or not based on its characteristics.
Context and schema related details of the data-set used in this example can be gathered from the following link:
https://archive.ics.uci.edu/ml/machine-learning-databases/car/car.names



```python
#loading the data-set:
#the data-set is available in csv format
#the data-set does not have header in it.
cars_df=spark.read.csv("/user/centos/car_evaluation.csv",header=False)
```


```python
#when data is loaded in the above way, the dataframe created has the following
#characteristics:
#1. All the columns are of type String
#2. The column names are arbitrary and have a naming convention of _cX.

#printing the column names:
cars_df.columns
```




    ['_c0', '_c1', '_c2', '_c3', '_c4', '_c5', '_c6']




```python
#printing the schema of the dataframe:
cars_df.printSchema()
```

    root
     |-- _c0: string (nullable = true)
     |-- _c1: string (nullable = true)
     |-- _c2: string (nullable = true)
     |-- _c3: string (nullable = true)
     |-- _c4: string (nullable = true)
     |-- _c5: string (nullable = true)
     |-- _c6: string (nullable = true)
    



```python
# its generally helpful to rename columns as it will facilitate the way
# queries are written. 
# to do so,creating a list to specify column names that will be used to 
# rename columns

col_names = ['buying','maint','doors','persons','lug_boot','safety',
             'acceptability']
```


```python
# one of the ways to rename columns of Spark SQL dataframe is:
# here *list notation used as a parameter means that elements of the list
# will be unpacked.
cars_df = cars_df.toDF(*col_names)
```


```python
# now if schema is printed, it shows that the dataframe has the columns with
# appropriate column names:
cars_df.printSchema()
```

    root
     |-- buying: string (nullable = true)
     |-- maint: string (nullable = true)
     |-- doors: string (nullable = true)
     |-- persons: string (nullable = true)
     |-- lug_boot: string (nullable = true)
     |-- safety: string (nullable = true)
     |-- acceptability: string (nullable = true)
    



```python
# previewing data to see how it looks like:
cars_df.head() # to get first row of the dataframe
```




    Row(buying=u'vhigh', maint=u'vhigh', doors=u'2', persons=u'2', lug_boot=u'small', safety=u'low', acceptability=u'unacc')




```python
# to show first 20 rows of the dataframe:
cars_df.show()
```

    +------+-----+-----+-------+--------+------+-------------+
    |buying|maint|doors|persons|lug_boot|safety|acceptability|
    +------+-----+-----+-------+--------+------+-------------+
    | vhigh|vhigh|    2|      2|   small|   low|        unacc|
    | vhigh|vhigh|    2|      2|   small|   med|        unacc|
    | vhigh|vhigh|    2|      2|   small|  high|        unacc|
    | vhigh|vhigh|    2|      2|     med|   low|        unacc|
    | vhigh|vhigh|    2|      2|     med|   med|        unacc|
    | vhigh|vhigh|    2|      2|     med|  high|        unacc|
    | vhigh|vhigh|    2|      2|     big|   low|        unacc|
    | vhigh|vhigh|    2|      2|     big|   med|        unacc|
    | vhigh|vhigh|    2|      2|     big|  high|        unacc|
    | vhigh|vhigh|    2|      4|   small|   low|        unacc|
    | vhigh|vhigh|    2|      4|   small|   med|        unacc|
    | vhigh|vhigh|    2|      4|   small|  high|        unacc|
    | vhigh|vhigh|    2|      4|     med|   low|        unacc|
    | vhigh|vhigh|    2|      4|     med|   med|        unacc|
    | vhigh|vhigh|    2|      4|     med|  high|        unacc|
    | vhigh|vhigh|    2|      4|     big|   low|        unacc|
    | vhigh|vhigh|    2|      4|     big|   med|        unacc|
    | vhigh|vhigh|    2|      4|     big|  high|        unacc|
    | vhigh|vhigh|    2|   more|   small|   low|        unacc|
    | vhigh|vhigh|    2|   more|   small|   med|        unacc|
    +------+-----+-----+-------+--------+------+-------------+
    only showing top 20 rows
    



```python
# persisting/caching the dataframe so that its DAG execution doesn't always
# initiate from the very starting point.
cars_df.persist()
# after persisting, an action like count or show has to be called to make
# caching "effective" because of Spark's lazy execution model.
cars_df.count() 
```




    1728




```python
# one of the many checks that should be performed prior to Classification tasks
# is to assess the proportion of each class label in the data-set
cars_df.groupBy("acceptability").count().show()
```

    +-------------+-----+
    |acceptability|count|
    +-------------+-----+
    |        unacc| 1210|
    |          acc|  384|
    |        vgood|   65|
    |         good|   69|
    +-------------+-----+
    



```python
# it appears that there are 4 class labels. It's a multi-class classification
# problem. The requirement is to just predict that whether a car is in 
# acceptable state or not so we'll convert this problem into a binary
# classification one.
```


```python
# One approach to do so would be to create a UDF that will encode the class
# to two labels 
from pyspark.sql.functions import udf
```


```python
# creating a python function that will return either unacc or acc.
# if class label is already unacc, it will return unacc. For all other cases,
# it will return acc.
def transform_class(given_status):
    if (given_status != "unacc"):
        return "acc"
    else:
        return "unacc"
```


```python
# registering the function as UDF so that it can be used in PySpark SQL:
transform_class_udf = udf(transform_class)
```


```python
# creating a new column with the encoded class consisting of two labels 
# using the UDF created previously:
cars_df.withColumn("new_class",transform_class_udf("acceptability")).show()
```

    +------+-----+-----+-------+--------+------+-------------+---------+
    |buying|maint|doors|persons|lug_boot|safety|acceptability|new_class|
    +------+-----+-----+-------+--------+------+-------------+---------+
    | vhigh|vhigh|    2|      2|   small|   low|        unacc|    unacc|
    | vhigh|vhigh|    2|      2|   small|   med|        unacc|    unacc|
    | vhigh|vhigh|    2|      2|   small|  high|        unacc|    unacc|
    | vhigh|vhigh|    2|      2|     med|   low|        unacc|    unacc|
    | vhigh|vhigh|    2|      2|     med|   med|        unacc|    unacc|
    | vhigh|vhigh|    2|      2|     med|  high|        unacc|    unacc|
    | vhigh|vhigh|    2|      2|     big|   low|        unacc|    unacc|
    | vhigh|vhigh|    2|      2|     big|   med|        unacc|    unacc|
    | vhigh|vhigh|    2|      2|     big|  high|        unacc|    unacc|
    | vhigh|vhigh|    2|      4|   small|   low|        unacc|    unacc|
    | vhigh|vhigh|    2|      4|   small|   med|        unacc|    unacc|
    | vhigh|vhigh|    2|      4|   small|  high|        unacc|    unacc|
    | vhigh|vhigh|    2|      4|     med|   low|        unacc|    unacc|
    | vhigh|vhigh|    2|      4|     med|   med|        unacc|    unacc|
    | vhigh|vhigh|    2|      4|     med|  high|        unacc|    unacc|
    | vhigh|vhigh|    2|      4|     big|   low|        unacc|    unacc|
    | vhigh|vhigh|    2|      4|     big|   med|        unacc|    unacc|
    | vhigh|vhigh|    2|      4|     big|  high|        unacc|    unacc|
    | vhigh|vhigh|    2|   more|   small|   low|        unacc|    unacc|
    | vhigh|vhigh|    2|   more|   small|   med|        unacc|    unacc|
    +------+-----+-----+-------+--------+------+-------------+---------+
    only showing top 20 rows
    



```python
# assigning that dataframe to a variable
cars_df_temp = cars_df.withColumn("acceptability_encoded",transform_class_udf("acceptability"))

```


```python
# creating a dataframe without the original "acceptability" column:
cars_df_2 = cars_df_temp.select([x for x in cars_df_temp.columns if x!="acceptability"])
```


```python
cars_df_2.show()
```

    +------+-----+-----+-------+--------+------+---------------------+
    |buying|maint|doors|persons|lug_boot|safety|acceptability_encoded|
    +------+-----+-----+-------+--------+------+---------------------+
    | vhigh|vhigh|    2|      2|   small|   low|                unacc|
    | vhigh|vhigh|    2|      2|   small|   med|                unacc|
    | vhigh|vhigh|    2|      2|   small|  high|                unacc|
    | vhigh|vhigh|    2|      2|     med|   low|                unacc|
    | vhigh|vhigh|    2|      2|     med|   med|                unacc|
    | vhigh|vhigh|    2|      2|     med|  high|                unacc|
    | vhigh|vhigh|    2|      2|     big|   low|                unacc|
    | vhigh|vhigh|    2|      2|     big|   med|                unacc|
    | vhigh|vhigh|    2|      2|     big|  high|                unacc|
    | vhigh|vhigh|    2|      4|   small|   low|                unacc|
    | vhigh|vhigh|    2|      4|   small|   med|                unacc|
    | vhigh|vhigh|    2|      4|   small|  high|                unacc|
    | vhigh|vhigh|    2|      4|     med|   low|                unacc|
    | vhigh|vhigh|    2|      4|     med|   med|                unacc|
    | vhigh|vhigh|    2|      4|     med|  high|                unacc|
    | vhigh|vhigh|    2|      4|     big|   low|                unacc|
    | vhigh|vhigh|    2|      4|     big|   med|                unacc|
    | vhigh|vhigh|    2|      4|     big|  high|                unacc|
    | vhigh|vhigh|    2|   more|   small|   low|                unacc|
    | vhigh|vhigh|    2|   more|   small|   med|                unacc|
    +------+-----+-----+-------+--------+------+---------------------+
    only showing top 20 rows
    



```python
# checking the proportion of class labels after the encoding:
cars_df_temp.groupBy("acceptability_encoded").count().show()
```

    +---------------------+-----+
    |acceptability_encoded|count|
    +---------------------+-----+
    |                unacc| 1210|
    |                  acc|  518|
    +---------------------+-----+
    



```python
# Using StringIndexer to encode the categorical variables to a numerical index:
from pyspark.ml.feature import StringIndexer
for col in cars_df_temp.columns:
    string_indexer=StringIndexer(inputCol=col,outputCol=col+"_index")
    model=string_indexer.fit(cars_df_temp)
    cars_df_temp=model.transform(cars_df_temp)
```


```python
cars_df_temp.printSchema()
# the dataframe will have all the original columns as well as the new
# columns with numerical indexed values in them.
```

    root
     |-- buying: string (nullable = true)
     |-- maint: string (nullable = true)
     |-- doors: string (nullable = true)
     |-- persons: string (nullable = true)
     |-- lug_boot: string (nullable = true)
     |-- safety: string (nullable = true)
     |-- acceptability: string (nullable = true)
     |-- acceptability_encoded: string (nullable = true)
     |-- buying_index: double (nullable = false)
     |-- maint_index: double (nullable = false)
     |-- doors_index: double (nullable = false)
     |-- persons_index: double (nullable = false)
     |-- lug_boot_index: double (nullable = false)
     |-- safety_index: double (nullable = false)
     |-- acceptability_index: double (nullable = false)
     |-- acceptability_encoded_index: double (nullable = false)
    



```python
# you don't need the original columns. So, filtering just those columns
# which has _index in them
col_list=filter(lambda x: "_index" in x,cars_df_temp.columns)
```


```python
# creating a dataframe consisting of just those columns which are indexed:
cars_df=cars_df_temp.select(col_list)
```


```python
cars_df.printSchema()
```

    root
     |-- buying_index: double (nullable = false)
     |-- maint_index: double (nullable = false)
     |-- doors_index: double (nullable = false)
     |-- persons_index: double (nullable = false)
     |-- lug_boot_index: double (nullable = false)
     |-- safety_index: double (nullable = false)
     |-- acceptability_index: double (nullable = false)
     |-- acceptability_encoded_index: double (nullable = false)
    



```python
cars_df.show(n=5,truncate=False)
```

    +------------+-----------+-----------+-------------+--------------+------------+-------------------+---------------------------+
    |buying_index|maint_index|doors_index|persons_index|lug_boot_index|safety_index|acceptability_index|acceptability_encoded_index|
    +------------+-----------+-----------+-------------+--------------+------------+-------------------+---------------------------+
    |3.0         |3.0        |2.0        |1.0          |0.0           |2.0         |0.0                |0.0                        |
    |3.0         |3.0        |2.0        |1.0          |0.0           |1.0         |0.0                |0.0                        |
    |3.0         |3.0        |2.0        |1.0          |0.0           |0.0         |0.0                |0.0                        |
    |3.0         |3.0        |2.0        |1.0          |1.0           |2.0         |0.0                |0.0                        |
    |3.0         |3.0        |2.0        |1.0          |1.0           |1.0         |0.0                |0.0                        |
    +------------+-----------+-----------+-------------+--------------+------------+-------------------+---------------------------+
    only showing top 5 rows
    



```python
# the categorical variables now have different levels (of numerical type)
# but for logistic regression, those categorical variables should be 
# transformed to dummy variables form. One of the ways, you can do so is 
# via OneHotEncoder. 
# that transformation need not be applied on the class label so excluding that:

feature_columns = [x for x in cars_df.columns if "acceptability" not in x]
```


```python
cars_df.count()
```




    1728




```python
feature_columns
```




    ['buying_index',
     'maint_index',
     'doors_index',
     'persons_index',
     'lug_boot_index',
     'safety_index']




```python
#now, applying OneHotEncoder:
from pyspark.ml.feature import OneHotEncoderEstimator
one_hot_encoder = OneHotEncoderEstimator(inputCols=[x for x in feature_columns],
                                        outputCols=[x+"_vec" for x in feature_columns])

# creating output columns, as a result of OneHotEncoderEstimator, which
# will have _vec at the end of them;
```


```python

one_hot_model=one_hot_encoder.fit(cars_df)
```


```python
cars_df2=one_hot_model.transform(cars_df)
```


```python
cars_df2.printSchema()
# now the dataframe has all the columns included OneHotEncoded:
```

    root
     |-- buying_index: double (nullable = false)
     |-- maint_index: double (nullable = false)
     |-- doors_index: double (nullable = false)
     |-- persons_index: double (nullable = false)
     |-- lug_boot_index: double (nullable = false)
     |-- safety_index: double (nullable = false)
     |-- acceptability_index: double (nullable = false)
     |-- acceptability_encoded_index: double (nullable = false)
     |-- persons_index_vec: vector (nullable = true)
     |-- doors_index_vec: vector (nullable = true)
     |-- safety_index_vec: vector (nullable = true)
     |-- lug_boot_index_vec: vector (nullable = true)
     |-- buying_index_vec: vector (nullable = true)
     |-- maint_index_vec: vector (nullable = true)
    



```python
# creating a dataframe consisting of just OneHotEncoded columns:
cars_df3=cars_df2.select([x for x in cars_df2.columns if "_vec" in x]+["acceptability_encoded_index"])
```


```python
cars_df3.printSchema()
```

    root
     |-- persons_index_vec: vector (nullable = true)
     |-- doors_index_vec: vector (nullable = true)
     |-- safety_index_vec: vector (nullable = true)
     |-- lug_boot_index_vec: vector (nullable = true)
     |-- buying_index_vec: vector (nullable = true)
     |-- maint_index_vec: vector (nullable = true)
     |-- acceptability_encoded_index: double (nullable = false)
    



```python
# creating feature vector of the features:
from pyspark.ml.linalg import Vectors
from pyspark.ml.feature import VectorAssembler
```


```python
cars_df.columns
```




    ['buying_index',
     'maint_index',
     'doors_index',
     'persons_index',
     'lug_boot_index',
     'safety_index',
     'acceptability_index',
     'acceptability_encoded_index']




```python
cars_df3.columns
```




    ['persons_index_vec',
     'doors_index_vec',
     'safety_index_vec',
     'lug_boot_index_vec',
     'buying_index_vec',
     'maint_index_vec',
     'acceptability_encoded_index']




```python
# creating a vector of features excluding the target class column:
v_assembler = VectorAssembler(inputCols=[x for x in cars_df3.columns if x != "acceptability_encoded_index"],
                            outputCol="features")
```


```python
cars_df4 = v_assembler.transform(cars_df3)
```


```python
cars_df4.show(n=3)
```

    +-----------------+---------------+----------------+------------------+----------------+---------------+---------------------------+--------------------+
    |persons_index_vec|doors_index_vec|safety_index_vec|lug_boot_index_vec|buying_index_vec|maint_index_vec|acceptability_encoded_index|            features|
    +-----------------+---------------+----------------+------------------+----------------+---------------+---------------------------+--------------------+
    |    (2,[1],[1.0])|  (3,[2],[1.0])|       (2,[],[])|     (2,[0],[1.0])|       (3,[],[])|      (3,[],[])|                        0.0|(15,[1,4,7],[1.0,...|
    |    (2,[1],[1.0])|  (3,[2],[1.0])|   (2,[1],[1.0])|     (2,[0],[1.0])|       (3,[],[])|      (3,[],[])|                        0.0|(15,[1,4,6,7],[1....|
    |    (2,[1],[1.0])|  (3,[2],[1.0])|   (2,[0],[1.0])|     (2,[0],[1.0])|       (3,[],[])|      (3,[],[])|                        0.0|(15,[1,4,5,7],[1....|
    +-----------------+---------------+----------------+------------------+----------------+---------------+---------------------------+--------------------+
    only showing top 3 rows
    



```python
cars_df4.printSchema()
```

    root
     |-- persons_index_vec: vector (nullable = true)
     |-- doors_index_vec: vector (nullable = true)
     |-- safety_index_vec: vector (nullable = true)
     |-- lug_boot_index_vec: vector (nullable = true)
     |-- buying_index_vec: vector (nullable = true)
     |-- maint_index_vec: vector (nullable = true)
     |-- acceptability_encoded_index: double (nullable = false)
     |-- features: vector (nullable = true)
    



```python
#doing logistic regression:
from pyspark.ml.classification import LogisticRegression
lr = LogisticRegression(maxIter=10,regParam=0.3,elasticNetParam=0.8, labelCol="label")
```


```python
output_df = cars_df4.withColumn("label",cars_df4.acceptability_encoded_index).select(["features","label"])
```


```python
output_df.show()
```

    +--------------------+-----+
    |            features|label|
    +--------------------+-----+
    |(15,[1,4,7],[1.0,...|  0.0|
    |(15,[1,4,6,7],[1....|  0.0|
    |(15,[1,4,5,7],[1....|  0.0|
    |(15,[1,4,8],[1.0,...|  0.0|
    |(15,[1,4,6,8],[1....|  0.0|
    |(15,[1,4,5,8],[1....|  0.0|
    |(15,[1,4],[1.0,1.0])|  0.0|
    |(15,[1,4,6],[1.0,...|  0.0|
    |(15,[1,4,5],[1.0,...|  0.0|
    |(15,[0,4,7],[1.0,...|  0.0|
    |(15,[0,4,6,7],[1....|  0.0|
    |(15,[0,4,5,7],[1....|  0.0|
    |(15,[0,4,8],[1.0,...|  0.0|
    |(15,[0,4,6,8],[1....|  0.0|
    |(15,[0,4,5,8],[1....|  0.0|
    |(15,[0,4],[1.0,1.0])|  0.0|
    |(15,[0,4,6],[1.0,...|  0.0|
    |(15,[0,4,5],[1.0,...|  0.0|
    |(15,[4,7],[1.0,1.0])|  0.0|
    |(15,[4,6,7],[1.0,...|  0.0|
    +--------------------+-----+
    only showing top 20 rows
    



```python
cars_df4.show(n=3)
```

    +-----------------+---------------+----------------+------------------+----------------+---------------+---------------------------+--------------------+
    |persons_index_vec|doors_index_vec|safety_index_vec|lug_boot_index_vec|buying_index_vec|maint_index_vec|acceptability_encoded_index|            features|
    +-----------------+---------------+----------------+------------------+----------------+---------------+---------------------------+--------------------+
    |    (2,[1],[1.0])|  (3,[2],[1.0])|       (2,[],[])|     (2,[0],[1.0])|       (3,[],[])|      (3,[],[])|                        0.0|(15,[1,4,7],[1.0,...|
    |    (2,[1],[1.0])|  (3,[2],[1.0])|   (2,[1],[1.0])|     (2,[0],[1.0])|       (3,[],[])|      (3,[],[])|                        0.0|(15,[1,4,6,7],[1....|
    |    (2,[1],[1.0])|  (3,[2],[1.0])|   (2,[0],[1.0])|     (2,[0],[1.0])|       (3,[],[])|      (3,[],[])|                        0.0|(15,[1,4,5,7],[1....|
    +-----------------+---------------+----------------+------------------+----------------+---------------+---------------------------+--------------------+
    only showing top 3 rows
    



```python
output_df.select("features").show(n=100,truncate=False) 
#in the output, it shows that features column is actually a sparse vector.
# length of the vector is 15. which corresponds to the 15 features. 
# and like its syntax, it shows where non-zero indices exist.
```

    +--------------------------------+
    |features                        |
    +--------------------------------+
    |(15,[1,4,7],[1.0,1.0,1.0])      |
    |(15,[1,4,6,7],[1.0,1.0,1.0,1.0])|
    |(15,[1,4,5,7],[1.0,1.0,1.0,1.0])|
    |(15,[1,4,8],[1.0,1.0,1.0])      |
    |(15,[1,4,6,8],[1.0,1.0,1.0,1.0])|
    |(15,[1,4,5,8],[1.0,1.0,1.0,1.0])|
    |(15,[1,4],[1.0,1.0])            |
    |(15,[1,4,6],[1.0,1.0,1.0])      |
    |(15,[1,4,5],[1.0,1.0,1.0])      |
    |(15,[0,4,7],[1.0,1.0,1.0])      |
    |(15,[0,4,6,7],[1.0,1.0,1.0,1.0])|
    |(15,[0,4,5,7],[1.0,1.0,1.0,1.0])|
    |(15,[0,4,8],[1.0,1.0,1.0])      |
    |(15,[0,4,6,8],[1.0,1.0,1.0,1.0])|
    |(15,[0,4,5,8],[1.0,1.0,1.0,1.0])|
    |(15,[0,4],[1.0,1.0])            |
    |(15,[0,4,6],[1.0,1.0,1.0])      |
    |(15,[0,4,5],[1.0,1.0,1.0])      |
    |(15,[4,7],[1.0,1.0])            |
    |(15,[4,6,7],[1.0,1.0,1.0])      |
    |(15,[4,5,7],[1.0,1.0,1.0])      |
    |(15,[4,8],[1.0,1.0])            |
    |(15,[4,6,8],[1.0,1.0,1.0])      |
    |(15,[4,5,8],[1.0,1.0,1.0])      |
    |(15,[4],[1.0])                  |
    |(15,[4,6],[1.0,1.0])            |
    |(15,[4,5],[1.0,1.0])            |
    |(15,[1,7],[1.0,1.0])            |
    |(15,[1,6,7],[1.0,1.0,1.0])      |
    |(15,[1,5,7],[1.0,1.0,1.0])      |
    |(15,[1,8],[1.0,1.0])            |
    |(15,[1,6,8],[1.0,1.0,1.0])      |
    |(15,[1,5,8],[1.0,1.0,1.0])      |
    |(15,[1],[1.0])                  |
    |(15,[1,6],[1.0,1.0])            |
    |(15,[1,5],[1.0,1.0])            |
    |(15,[0,7],[1.0,1.0])            |
    |(15,[0,6,7],[1.0,1.0,1.0])      |
    |(15,[0,5,7],[1.0,1.0,1.0])      |
    |(15,[0,8],[1.0,1.0])            |
    |(15,[0,6,8],[1.0,1.0,1.0])      |
    |(15,[0,5,8],[1.0,1.0,1.0])      |
    |(15,[0],[1.0])                  |
    |(15,[0,6],[1.0,1.0])            |
    |(15,[0,5],[1.0,1.0])            |
    |(15,[7],[1.0])                  |
    |(15,[6,7],[1.0,1.0])            |
    |(15,[5,7],[1.0,1.0])            |
    |(15,[8],[1.0])                  |
    |(15,[6,8],[1.0,1.0])            |
    |(15,[5,8],[1.0,1.0])            |
    |(15,[],[])                      |
    |(15,[6],[1.0])                  |
    |(15,[5],[1.0])                  |
    |(15,[1,3,7],[1.0,1.0,1.0])      |
    |(15,[1,3,6,7],[1.0,1.0,1.0,1.0])|
    |(15,[1,3,5,7],[1.0,1.0,1.0,1.0])|
    |(15,[1,3,8],[1.0,1.0,1.0])      |
    |(15,[1,3,6,8],[1.0,1.0,1.0,1.0])|
    |(15,[1,3,5,8],[1.0,1.0,1.0,1.0])|
    |(15,[1,3],[1.0,1.0])            |
    |(15,[1,3,6],[1.0,1.0,1.0])      |
    |(15,[1,3,5],[1.0,1.0,1.0])      |
    |(15,[0,3,7],[1.0,1.0,1.0])      |
    |(15,[0,3,6,7],[1.0,1.0,1.0,1.0])|
    |(15,[0,3,5,7],[1.0,1.0,1.0,1.0])|
    |(15,[0,3,8],[1.0,1.0,1.0])      |
    |(15,[0,3,6,8],[1.0,1.0,1.0,1.0])|
    |(15,[0,3,5,8],[1.0,1.0,1.0,1.0])|
    |(15,[0,3],[1.0,1.0])            |
    |(15,[0,3,6],[1.0,1.0,1.0])      |
    |(15,[0,3,5],[1.0,1.0,1.0])      |
    |(15,[3,7],[1.0,1.0])            |
    |(15,[3,6,7],[1.0,1.0,1.0])      |
    |(15,[3,5,7],[1.0,1.0,1.0])      |
    |(15,[3,8],[1.0,1.0])            |
    |(15,[3,6,8],[1.0,1.0,1.0])      |
    |(15,[3,5,8],[1.0,1.0,1.0])      |
    |(15,[3],[1.0])                  |
    |(15,[3,6],[1.0,1.0])            |
    |(15,[3,5],[1.0,1.0])            |
    |(15,[1,2,7],[1.0,1.0,1.0])      |
    |(15,[1,2,6,7],[1.0,1.0,1.0,1.0])|
    |(15,[1,2,5,7],[1.0,1.0,1.0,1.0])|
    |(15,[1,2,8],[1.0,1.0,1.0])      |
    |(15,[1,2,6,8],[1.0,1.0,1.0,1.0])|
    |(15,[1,2,5,8],[1.0,1.0,1.0,1.0])|
    |(15,[1,2],[1.0,1.0])            |
    |(15,[1,2,6],[1.0,1.0,1.0])      |
    |(15,[1,2,5],[1.0,1.0,1.0])      |
    |(15,[0,2,7],[1.0,1.0,1.0])      |
    |(15,[0,2,6,7],[1.0,1.0,1.0,1.0])|
    |(15,[0,2,5,7],[1.0,1.0,1.0,1.0])|
    |(15,[0,2,8],[1.0,1.0,1.0])      |
    |(15,[0,2,6,8],[1.0,1.0,1.0,1.0])|
    |(15,[0,2,5,8],[1.0,1.0,1.0,1.0])|
    |(15,[0,2],[1.0,1.0])            |
    |(15,[0,2,6],[1.0,1.0,1.0])      |
    |(15,[0,2,5],[1.0,1.0,1.0])      |
    |(15,[2,7],[1.0,1.0])            |
    +--------------------------------+
    only showing top 100 rows
    



```python
output_df.groupBy("label").count().show()
```

    +-----+-----+
    |label|count|
    +-----+-----+
    |  0.0| 1210|
    |  1.0|  518|
    +-----+-----+
    



```python
# as there is a lot of class imbalance so doing stratified sampling:
output_df_st= output_df.sampleBy('label',fractions = {0: 518./1210,1:1.0})
```


```python
# again comparing the proportion of class labels in 
# stratified sampled dataframe to se
output_df_st.groupBy("label").count().show()
```

    +-----+-----+
    |label|count|
    +-----+-----+
    |  0.0|  506|
    |  1.0|  518|
    +-----+-----+
    



```python
# splitting data into training and test set:
train_dfs,test_dfs=output_df_st.randomSplit([0.7,0.3],seed=1000)
```


```python
# fitting logistic regression model on the training set:
lrModel = lr.fit(train_dfs)
```


```python
lrModel.numFeatures # 15 features are there;
```




    15




```python
import pyspark.sql.functions as funct

```


```python
col2=cars_df_temp.columns
```


```python
# getting the co-efficients and intercept:
lrModel.coefficients
lrModel.intercept
```




    0.062144265838000264




```python
lrModel.coefficients
```




    SparseVector(15, {1: -0.2489})




```python
training_summary = lrModel.summary
```


```python
# accuracy on training set:
training_summary.accuracy
```




    0.7293956043956044




```python
# making predictions on the test set:
test_model = lrModel.transform(test_dfs)
```


```python
test_model.show()
```

    +--------------------+-----+--------------------+--------------------+----------+
    |            features|label|       rawPrediction|         probability|prediction|
    +--------------------+-----+--------------------+--------------------+----------+
    |(15,[0,2,5,7],[1....|  0.0|[-0.0621442658380...|[0.48446893151758...|       1.0|
    |(15,[0,2,5,7,9,14...|  1.0|[-0.0621442658380...|[0.48446893151758...|       1.0|
    |(15,[0,2,5,7,10,1...|  1.0|[-0.0621442658380...|[0.48446893151758...|       1.0|
    |(15,[0,2,5,7,10,1...|  1.0|[-0.0621442658380...|[0.48446893151758...|       1.0|
    |(15,[0,2,5,7,11],...|  1.0|[-0.0621442658380...|[0.48446893151758...|       1.0|
    |(15,[0,2,5,7,11,1...|  1.0|[-0.0621442658380...|[0.48446893151758...|       1.0|
    |(15,[0,2,5,7,14],...|  1.0|[-0.0621442658380...|[0.48446893151758...|       1.0|
    |(15,[0,2,5,8,9,12...|  1.0|[-0.0621442658380...|[0.48446893151758...|       1.0|
    |(15,[0,2,5,8,9,14...|  1.0|[-0.0621442658380...|[0.48446893151758...|       1.0|
    |(15,[0,2,5,8,10,1...|  1.0|[-0.0621442658380...|[0.48446893151758...|       1.0|
    |(15,[0,2,5,8,11,1...|  1.0|[-0.0621442658380...|[0.48446893151758...|       1.0|
    |(15,[0,2,5,8,11,1...|  1.0|[-0.0621442658380...|[0.48446893151758...|       1.0|
    |(15,[0,2,5,9,14],...|  1.0|[-0.0621442658380...|[0.48446893151758...|       1.0|
    |(15,[0,2,5,10],[1...|  1.0|[-0.0621442658380...|[0.48446893151758...|       1.0|
    |(15,[0,2,5,10,12]...|  1.0|[-0.0621442658380...|[0.48446893151758...|       1.0|
    |(15,[0,2,5,11],[1...|  1.0|[-0.0621442658380...|[0.48446893151758...|       1.0|
    |(15,[0,2,5,11,12]...|  1.0|[-0.0621442658380...|[0.48446893151758...|       1.0|
    |(15,[0,2,5,11,13]...|  1.0|[-0.0621442658380...|[0.48446893151758...|       1.0|
    |(15,[0,2,6],[1.0,...|  0.0|[-0.0621442658380...|[0.48446893151758...|       1.0|
    |(15,[0,2,6,7,11,1...|  1.0|[-0.0621442658380...|[0.48446893151758...|       1.0|
    +--------------------+-----+--------------------+--------------------+----------+
    only showing top 20 rows
    



```python
#evaluating:
import pyspark.ml.evaluation as ev
evaluator = ev.BinaryClassificationEvaluator(rawPredictionCol='probability',labelCol="label")
```


```python
evaluator.evaluate(test_model)
```




    0.7167832167832168




```python
print(evaluator.evaluate(test_model, 
    {evaluator.metricName: 'areaUnderROC'}))
```

    0.716783216783



```python
# use RandomForest here:
# refer to Spark Programming Guide for reference.
```

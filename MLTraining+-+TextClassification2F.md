
## Text Classification
This notebook addresses how you can perform text classification using machine learning techniques.
The problem at hand involves classifying jobs posted on a job portal to their respective categories. 
The data-set has the following characteristics:
It has 4 columns:
id
title (title of the job posted on a website)
text_description (job description)
category (category of the posted job)

and the company is working on automatic tagger so that future jobs are automatically tagged with the right category without user's intervention


```python
# reading the data in Spark as dataframe:
df = spark.read.csv("/user/centos/job_data_4.csv",header=True)
```


```python
# previewing the data:
df.show()
```

    +---+--------------------+--------------------+--------+
    |_c0|               title|    text_description|category|
    +---+--------------------+--------------------+--------+
    |  1|A Genuine Home Bu...|Are you looking f...|   sales|
    |  2|    Sales Consultant|"Our client is a ...|   sales|
    |  3|TRAINEE SALES MAR...|Are you creative ...|   sales|
    |  4|Trainee Sales/Mar...|Are you a custome...|   sales|
    |  5| Retention Executive|Recognised as the...|   sales|
    |  6|Sales Representat...|Our client is one...|   sales|
    |  7|Seeking Hungry In...|Listing InfoWe ar...|   sales|
    |  8|Education Sales C...|This is a new and...|   sales|
    |  9|WORK AT HOME MUMS...|Make Your Ability...|   sales|
    | 10|SME Business Sale...|We're currently s...|   sales|
    | 11|  Subscription Sales|"With a massive s...|   sales|
    | 12|Area Manager - SALES|Do you come from ...|   sales|
    | 13|A Genuine Home Bu...|Are you looking f...|   sales|
    | 14|       Sales Manager|"Our client is a ...|   sales|
    | 15|    Sales Consultant|"Listing InfoOur ...|   sales|
    | 16|Trainee Sales Con...|This Company are ...|   sales|
    | 17|Work From Home - ...|Listing InfoLearn...|   sales|
    | 18|Home Based Busine...|Are you serious a...|   sales|
    | 19|CALLING ALL GRADU...|This is a univers...|   sales|
    | 20|Make Your Sales S...|Are you looking f...|   sales|
    +---+--------------------+--------------------+--------+
    only showing top 20 rows
    



```python
# Checking for duplicate rows:
num_rows = df.count()
num_rows_distinct = df.distinct().count()
num_rows - num_rows_distinct
```




    0




```python
#there appears to be no duplicates in data.
#checking for missing values:

import pyspark.sql.functions as funct

#creating a function that will display a dataframe showing number of missing values in each column: 
def check_missing(given_dataframe):
    num_rows = given_dataframe.count()
    given_dataframe.agg(*[(num_rows - funct.count(col)).alias(col+"_missing") for col in given_dataframe.columns]).show()
```


```python
check_missing(df)
```

    +-----------+-------------+------------------------+----------------+
    |_c0_missing|title_missing|text_description_missing|category_missing|
    +-----------+-------------+------------------------+----------------+
    |          0|            0|                       1|               0|
    +-----------+-------------+------------------------+----------------+
    



```python
# it shows that one of the value in text_description column is missing. 
# subsetting just that row to see how it looks like:
df.where("text_description is null").show()
```

    +----+--------------------+----------------+--------------+
    | _c0|               title|text_description|      category|
    +----+--------------------+----------------+--------------+
    |3537|Student Services ...|            null|administrative|
    +----+--------------------+----------------+--------------+
    



```python
# dropping that row:
df2 = df.where("_c0 != 3537")
```


```python
# checking the count again:
df2.count()
```




    5837




```python
# checking if there are any missing values:
check_missing(df2)
```

    +-----------+-------------+------------------------+----------------+
    |_c0_missing|title_missing|text_description_missing|category_missing|
    +-----------+-------------+------------------------+----------------+
    |          0|            0|                       0|               0|
    +-----------+-------------+------------------------+----------------+
    



```python
# as a part of pre-processing and to reduce the feature-space, one of the practices is to convert all the words
# to lower-case so that no different features are created for the same word but in different cases:
# try to convert all the data to lower-case:

#it can be done by using funct.lower function (where funct is a label for pyspark.sql.functions)
df2.select(funct.lower(df2.title).alias("title"),funct.lower(df2.text_description).alias("text_description")\
           ,funct.lower(df2.category).alias("category")).show()
```

    +--------------------+--------------------+--------+
    |               title|    text_description|category|
    +--------------------+--------------------+--------+
    |a genuine home bu...|are you looking f...|   sales|
    |    sales consultant|"our client is a ...|   sales|
    |trainee sales mar...|are you creative ...|   sales|
    |trainee sales/mar...|are you a custome...|   sales|
    | retention executive|recognised as the...|   sales|
    |sales representat...|our client is one...|   sales|
    |seeking hungry in...|listing infowe ar...|   sales|
    |education sales c...|this is a new and...|   sales|
    |work at home mums...|make your ability...|   sales|
    |sme business sale...|we're currently s...|   sales|
    |  subscription sales|"with a massive s...|   sales|
    |area manager - sales|do you come from ...|   sales|
    |a genuine home bu...|are you looking f...|   sales|
    |       sales manager|"our client is a ...|   sales|
    |    sales consultant|"listing infoour ...|   sales|
    |trainee sales con...|this company are ...|   sales|
    |work from home - ...|listing infolearn...|   sales|
    |home based busine...|are you serious a...|   sales|
    |calling all gradu...|this is a univers...|   sales|
    |make your sales s...|are you looking f...|   sales|
    +--------------------+--------------------+--------+
    only showing top 20 rows
    



```python
# as the previous output looks good, so storing the results to a dataframe:
# also assigning aliases of column names accordingly:
df3 = df2.select(funct.lower(df2.title).alias("title"),funct.lower(df2.text_description).alias("text_description")\
           ,funct.lower(df2.category).alias("category"))
```


```python
# as both title and text description column may contain important signals, so creating one column consisting of
# concatenation of both:
```


```python
df4=df3.withColumn("text_data",funct.concat(funct.col("title"), funct.lit(" "), funct.col("text_description"))).select("text_data","category")
```


```python
df4.persist().count()
```




    5837




```python
df4.columns
```




    ['text_data', 'category']




```python
df4.show(3)
```

    +--------------------+--------+
    |           text_data|category|
    +--------------------+--------+
    |a genuine home bu...|   sales|
    |sales consultant ...|   sales|
    |trainee sales mar...|   sales|
    +--------------------+--------+
    only showing top 3 rows
    



```python
# For further pre-processing, performing tokenization of the text data and creating a new column (text_data_t) containing
# tokenized data:
from pyspark.ml.feature import Tokenizer

tokenizer = Tokenizer(inputCol="text_data",outputCol="text_data_t")
df5=tokenizer.transform(df4)
```


```python
df5.select(funct.size("text_data_t")).show() #checking the size of each row of the tokenized column as it will 
# be helpful in understanding that once stopped words are removed, it will be reduced.
```

    +-----------------+
    |size(text_data_t)|
    +-----------------+
    |              460|
    |              172|
    |              419|
    |              369|
    |              340|
    |              276|
    |              242|
    |              265|
    |              404|
    |              238|
    |              202|
    |              149|
    |              460|
    |              175|
    |              174|
    |              185|
    |              523|
    |              456|
    |              223|
    |              463|
    +-----------------+
    only showing top 20 rows
    



```python
# Removing StopWords:
from pyspark.ml.feature import StopWordsRemover, CountVectorizer, IndexToString
stop_words = StopWordsRemover(inputCol="text_data_t",outputCol="text_data_ts")

```


```python
df6=stop_words.transform(df5)
```


```python
df6.select(funct.size("text_data_ts")).show() # comparing with the previous output, can validate that length is shorter now;
# after removing stop words
```

    +------------------+
    |size(text_data_ts)|
    +------------------+
    |               257|
    |               112|
    |               259|
    |               208|
    |               216|
    |               179|
    |               150|
    |               161|
    |               253|
    |               165|
    |               116|
    |                99|
    |               257|
    |               120|
    |               113|
    |               133|
    |               294|
    |               284|
    |               136|
    |               261|
    +------------------+
    only showing top 20 rows
    



```python
# Using CountVectorizer transformer to form bag of words:
from pyspark.ml.feature import  CountVectorizer
count_vectorizer = CountVectorizer(inputCol="text_data_ts",outputCol="features")
```


```python
# also performing string indexing of the category column to convert string categories to numeric ones:
from pyspark.ml.feature import StringIndexer
string_indexer = StringIndexer(inputCol="category",outputCol="category_index")
```


```python
df7=string_indexer.fit(df6).transform(df6)
```


```python
df7b=count_vectorizer.fit(df7).transform(df7)
```


```python
df7b.show(3)
```

    +--------------------+--------+--------------------+--------------------+--------------+--------------------+
    |           text_data|category|         text_data_t|        text_data_ts|category_index|            features|
    +--------------------+--------+--------------------+--------------------+--------------+--------------------+
    |a genuine home bu...|   sales|[a, genuine, home...|[genuine, home, b...|           1.0|(25074,[0,1,2,3,5...|
    |sales consultant ...|   sales|[sales, consultan...|[sales, consultan...|           1.0|(25074,[0,2,4,6,7...|
    |trainee sales mar...|   sales|[trainee, sales, ...|[trainee, sales, ...|           1.0|(25074,[0,1,2,3,4...|
    +--------------------+--------+--------------------+--------------------+--------------+--------------------+
    only showing top 3 rows
    



```python
# renaming the category_index column to label:
df8=df7b.withColumn("label",df7.category_index)
```


```python
# splitting the data into training and test data:
train_df,test_df = df8.randomSplit([0.7,0.3])
```


```python
rf_model = RandomForestClassifier(labelCol="label", featuresCol="features", numTrees=50)
results_rf = rf_model.fit(train_df).transform(test_df)
```


```python
import pyspark.ml.evaluation as ev
```


```python
evaluator=ev.MulticlassClassificationEvaluator(labelCol="label",predictionCol="prediction"\
                                              ,metricName="accuracy")
```


```python
evaluator.evaluate(results_rf) # evaluating the random forest model:
```




    0.7921503593145384




```python
# to do: 
# try naive bayes:
```


```python
# to do:
# try decision trees:
```
